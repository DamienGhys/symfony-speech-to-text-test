<?php
// src/Controller/LuckyNumberController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class AudioStreamingController extends AbstractController {

    /**
     * @Route("/audiorecord", name="audiorecord")
     */
    public function testSoundRecord() {

        $process = new Process(['python3', '../src/Python/sounddevice-record.py']);
        $process->run();

        if(!$process->isSuccessful()) throw new ProcessFailedException($process);
        return new Response();
    }
}