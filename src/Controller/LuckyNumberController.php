<?php
// src/Controller/LuckyNumberController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LuckyNumberController extends AbstractController {

    /**
     * @Route("/luckynumber", name="luckynumber")
     */
    public function luckyNumber() : Response {

        $number = $this->luckyNumberGenerator();

        return $this->render('lucky/number.html.twig', [
            'number' => $number
        ]);
    }

    private function luckyNumberGenerator() : int {
        
        /**
         * @var integer
         * @range(0, 1000)
         * @label('A random number')
         */
        $number = random_int(0, 1000);

        return $number;
    }
}